# Install some packages to do stuff
import pandas as pd  # Excel/SQL style data tool... Really powerful
import matplotlib.pyplot as plt  # Plotting base
from sklearn.cluster import KMeans
import numpy as np

# Set the plotting size default
fig = plt.figure(figsize=(15, 7.5))
ax = fig.add_subplot(111)

# Read in the data as a DataFrame
df = pd.read_csv("data.csv")

# Seperate our data
data = df.drop("OBS", axis=1).copy()

# Get our starting centroids
starting = data[0:1].append(data[3:4])

# Create an empty KMeans object with starting centroids
kmeans = KMeans(n_clusters=2, init=starting)

# Fit the KMeans object to our data
kmeans.fit(data)

# Step size of the mesh. Decrease to increase the quality of the VQ.
h = 0.01

# Plot the decision boundary. For that, we will assign a color to each
x_min, x_max = data["x1"].min() - 1, data["x1"].max() + 1
y_min, y_max = data["x2"].min() - 1, data["x2"].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

# Obtain labels for each point in mesh. Use last trained model.
Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure(1)

# Show the colored regions
plt.imshow(
    Z,
    interpolation="nearest",
    extent=(xx.min(), xx.max(), yy.min(), yy.max()),
    cmap=plt.cm.Paired,
    aspect="auto",
    origin="lower",
)
# Plot the data points in space
plt.plot(data["x1"], data["x2"], "k.", markersize=5)
# Plot the centroids as a star
centroids = kmeans.cluster_centers_
plt.scatter(
    centroids[:, 0],
    centroids[:, 1],
    marker="*",
    s=50,
    linewidths=1,
    color="w",
    zorder=10,
)
# Label the data points with their names
for row in df.iterrows():
    ax.annotate(row[1][0], xy=(row[1][1], row[1][2]), textcoords="data")

# Label the cnetroids with their values
for centroid in centroids:
    ax.annotate(f"{centroid[0]}, {centroid[1]}", xy=centroid, textcoords="data")

# Make a title
plt.title(
    "K-means clustering with color coded regions \nCentroids are marked with star"
)
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
# Show the plot!
plt.show()
