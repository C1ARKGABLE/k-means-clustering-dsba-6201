# K-Means Clustering in Python

## This repo is a sample of what a K-Means clustering algorithm looks like.

## The final output:
![A pretty scatter plot with K-Means Clustering](output.png)

### Note: Do not use this code in production! 

